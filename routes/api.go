package routes

import (
	"github.com/gin-gonic/gin"
	"golangexample/controllers"
)

func Route() {
	authMiddleware := controllers.AuthMiddleware()

	router := gin.Default()
	router.POST("/login", authMiddleware.LoginHandler)
	v1 := router.Group("/api/v1")
	{
		v1.Use(authMiddleware.MiddlewareFunc())
		v1.GET("/me", controllers.GetMe)

		todos := v1.Group("/todos")
		{
			TodoController := controllers.TodoController{}
			todos.GET("/", TodoController.List)
			todos.POST("/", TodoController.Create)
			todos.GET("/:id", TodoController.Show)
			todos.PUT("/:id", TodoController.Update)
			todos.DELETE("/:id", TodoController.Delete)
		}
	}
	router.Run()
}

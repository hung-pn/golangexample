package controllers

import (
	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
	"golangexample/db"
	"golangexample/models"
	"golangexample/validations"
	"net/http"
	"time"
)

var identityKey = "username"

func AuthMiddleware() *jwt.GinJWTMiddleware {
	db := db.GetDB()
	authMiddleware, _ := jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "test zone",
		Key:         []byte("secret key"),
		Timeout:     time.Hour,
		MaxRefresh:  time.Hour,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*models.UserModel); ok {
				return jwt.MapClaims{
					identityKey: v.Username,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &models.UserModel{
				Username: claims[identityKey].(string),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var loginVals validations.Login
			var user models.UserModel
			if err := c.ShouldBind(&loginVals); err != nil {
				return "", jwt.ErrMissingLoginValues
			}
			username := loginVals.Username
			password := loginVals.Password

			db.Where("username = ?", username).First(&user)
			if user.Password == password {
				return &models.UserModel{
					Username: user.Username,
					Password: user.Password,
				}, nil
			}

			return nil, jwt.ErrFailedAuthentication
		},
	})
	return authMiddleware
}

func GetMe(c *gin.Context) {
	claims := jwt.ExtractClaims(c)
	user, _ := c.Get("username")
	c.JSON(http.StatusOK, gin.H{
		"userID":   claims["username"],
		"userName": user.(*models.UserModel).Username,
		"text":     "Hello World.",
	})
}

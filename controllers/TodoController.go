package controllers

import (
	"github.com/gin-gonic/gin"
	"golangexample/repositories"
	"net/http"
	"strconv"
)

type TodoController struct{}

func (t TodoController) List(c *gin.Context) {
	todoRes := repositories.TodoRepository{}
	todos := todoRes.GetListTodo()
	c.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data":   todos,
	})
}

func (t TodoController) Create(c *gin.Context) {
	todoRes := repositories.TodoRepository{}
	todoRes.CreateTodo(c)
	c.JSON(http.StatusCreated, gin.H{
		"status":  http.StatusCreated,
		"message": "Created successfully!",
	})
}

func (t TodoController) Show(c *gin.Context) {
	todoId, _ := strconv.Atoi(c.Param("id"))
	todoRes := repositories.TodoRepository{}
	todo := todoRes.FindTodo(c, todoId)
	c.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data":   todo,
	})
}

func (t TodoController) Update(c *gin.Context) {
	todoId, _ := strconv.Atoi(c.Param("id"))
	todoRes := repositories.TodoRepository{}
	todo := todoRes.UpdateTodo(c, todoId)
	c.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data":   todo,
	})
}

func (t TodoController) Delete(c *gin.Context) {
	todoId, _ := strconv.Atoi(c.Param("id"))
	todoRes := repositories.TodoRepository{}
	todoRes.DeleteTodo(c, todoId)
	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "Deleted Successfully!",
	})
}

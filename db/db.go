package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/joho/godotenv"
	"golangexample/models"
	"log"
	"os"
)

func loadENV() {
	errENV := godotenv.Load()
	if errENV != nil {
		log.Fatal("Error loading .env file")
	}
}

var db *gorm.DB

func init() {
	loadENV()
	//open a db connection
	var errGORM error
	db, errGORM = gorm.Open(os.Getenv("DB_DRIVER"), fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local",
		os.Getenv("DB_USERNAME"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_DATABASE"),
	))
	if errGORM != nil {
		log.Fatal("failed to connect database")
	}
	//Migrate the schema
	db.AutoMigrate(&models.TodoModel{})
	db.AutoMigrate(&models.UserModel{})
}
func GetDB() *gorm.DB {
	return db
}

package models

import "github.com/jinzhu/gorm"

type (
	// todoModel describes a todoModel type
	TodoModel struct {
		gorm.Model
		Title     string `json:"title"`
		Completed int    `json:"completed"`
	}
)

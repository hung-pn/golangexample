package models

import "github.com/jinzhu/gorm"

type (
	// todoModel describes a userModel type
	UserModel struct {
		gorm.Model
		Username string `json:"username"`
		Password string `json:"password"`
	}
)

package repositories

import (
	"github.com/gin-gonic/gin"
	"golangexample/db"
	"golangexample/models"
	"strconv"
)

type TodoRepository struct{}

func (t TodoRepository) GetListTodo() []models.TodoModel {
	db := db.GetDB()
	var todos []models.TodoModel
	db.Find(&todos)
	return todos
}

func (t TodoRepository) CreateTodo(c *gin.Context) models.TodoModel {
	db := db.GetDB()
	completed, _ := strconv.Atoi(c.PostForm("completed"))
	todo := models.TodoModel{Title: c.PostForm("title"), Completed: completed}
	db.Save(&todo)
	return todo
}

func (t TodoRepository) FindTodo(c *gin.Context, id int) models.TodoModel {
	db := db.GetDB()
	var todo models.TodoModel
	db.Find(&todo, id)
	return todo
}

func (t TodoRepository) UpdateTodo(c *gin.Context, id int) models.TodoModel {
	db := db.GetDB()
	var todo models.TodoModel
	db.Find(&todo, id)
	db.Model(&todo).Updates(map[string]interface{}{
		"title": c.PostForm("title"),
	})
	return todo
}

func (t TodoRepository) DeleteTodo(c *gin.Context, id int) models.TodoModel {
	db := db.GetDB()
	var todo models.TodoModel
	db.Find(&todo, id)
	db.Delete(&todo)
	return todo
}
